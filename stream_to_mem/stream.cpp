#include <queue>
#include <mutex>
#include <iostream>

#include <condition_variable>
#include <vlc/vlc.h>
#include <opencv2/core/core.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/objdetect/objdetect.hpp>
#include <opencv2/highgui/highgui.hpp>

#define NB_ALLOCATED_PICTURE 2


/**
 * @brief The FaceDetectorPlayer  allows to read a video from a file and run a
 * face detection algorithm on each frames.
 *
 * The goal is to present how libvlc can be setup to render video to custom memory
 */
class FaceDetectorPlayer
{
public:
    FaceDetectorPlayer( libvlc_instance_t *vlcInstance, const char *filePath );

    ~FaceDetectorPlayer();

    //this method will start the video, and block until the end of the media or on user request
    int play();

private:
    //VLC callbacks
    static void on_events( const struct libvlc_event_t *event, void *opaque );
    static void *on_video_lock ( void *opaque, void **planes );
    static void on_video_unlock( void *opaque, void *picture, void *const *planes );
    static void on_video_display( void *opaque, void *picture );
    static unsigned int on_format_setup( void **opaque, char *chroma,
                                         unsigned *width, unsigned *height,
                                         unsigned *pitches, unsigned *lines );
    static void on_format_cleanup( void  *opaque );

    //internal message definition to communicate between vlc video thread and the main thread
    enum class PlayerAction
    {
        CONTINUE,
        QUIT_OK,
        QUIT_KO,
        STOP
    };

    //send custom notification to our main thread
    void notify_action( PlayerAction action );

private:
    //lib vlc objects
    libvlc_media_t *m_media;
    libvlc_media_player_t *m_mediaplayer;

    //m_action and associated mutex, allow simple message passing bewteen vlc video thread and our main thread
    PlayerAction m_action;
    std::mutex  m_action_mutex;
    std::condition_variable m_playcond;

    //this queue will holds a list of available buffers
    std::vector<cv::Mat *> m_image_queue;
    //mutex to protect access to m_image_queue and image buffer
    std::mutex m_image_queue_lock;

    //OpenCV related objects (temporary image and classifier)
    cv::Mat m_gray_image;
    cv::CascadeClassifier  face_cascade;
};

//Ctor
FaceDetectorPlayer::FaceDetectorPlayer( libvlc_instance_t *vlcInstance, const char *filePath ):
    m_media(nullptr),
    m_mediaplayer(nullptr),
    m_action(PlayerAction::CONTINUE)
{
    //create a media from our input file
    m_media = libvlc_media_new_path( vlcInstance, filePath );
    if ( m_media == nullptr )
    {
        std::string reason( "Invalid media: " );
        reason +=   filePath;
        throw std::runtime_error( reason );
    }

    //initialise a media player from our media
    m_mediaplayer = libvlc_media_player_new_from_media( m_media );
    if ( m_mediaplayer == nullptr )
        throw std::runtime_error( "unable to initialise the the media player" );

    //Here we register two sets of callbacks:
    //the first one will be called to setup the format of the video
    libvlc_video_set_format_callbacks( m_mediaplayer, on_format_setup, on_format_cleanup );
    //the second defines the methods that will be called while for rendering and presenting each images.
    libvlc_video_set_callbacks( m_mediaplayer, on_video_lock, on_video_unlock, on_video_display, this );

    //register callbacks to player manager
    libvlc_event_manager_t *mgr = libvlc_media_player_event_manager( m_mediaplayer );
    libvlc_event_attach( mgr, libvlc_MediaPlayerEndReached, on_events, this );
    libvlc_event_attach( mgr, libvlc_MediaPlayerEncounteredError, on_events, this );

    //load the default face classifier provided by opencv, path may vary
    face_cascade = cv::CascadeClassifier( "/usr/share/opencv/haarcascades/haarcascade_frontalface_default.xml" );
    //create opencv window output
    cv::namedWindow( "video" );
}

//Dtor
FaceDetectorPlayer::~FaceDetectorPlayer()
{
    cv::destroyWindow( "video" );
    on_format_cleanup( this );
    libvlc_media_release( m_media );
}

int FaceDetectorPlayer::play()
{
    std::unique_lock<std::mutex> lock( m_action_mutex );
    m_action = PlayerAction::CONTINUE;

    //start the playback
    libvlc_media_player_play( m_mediaplayer );
    //we wait for the program to end
    while ( m_action == PlayerAction::CONTINUE )
    {
        m_playcond.wait( lock );
        switch ( m_action )
        {
            case PlayerAction::STOP:
                libvlc_media_player_stop( m_mediaplayer );
                break;
            case PlayerAction::QUIT_OK:
                return 0;
            case PlayerAction::QUIT_KO:
                return 1;
            default:
                break;
        }
    }

    return 0;
}

//callback called when libvlc sends us events (see libvlc_event_attach)
void FaceDetectorPlayer::on_events( const libvlc_event_t *event, void *opaque )
{
    assert( opaque );
    FaceDetectorPlayer *that = reinterpret_cast<FaceDetectorPlayer *>( opaque );
    switch ( event->type )
    {
        case libvlc_MediaPlayerEndReached:
            {
                std::cout << "EOS" << std::endl;
                //the end of the media has been reached, we can now unlock our play function and quit the program
                that->notify_action( PlayerAction::STOP );
                break;
            }
        case libvlc_MediaPlayerEncounteredError:
            {
                std::cerr << "video player encountered an error" << std::endl;
                that->notify_action( PlayerAction::QUIT_KO );
                break;
            }
        default:
            break;
    }
}

//this callback is called before the image is decoded
void *FaceDetectorPlayer::on_video_lock ( void *opaque, void **planes )
{
    assert( opaque );
    FaceDetectorPlayer *that = reinterpret_cast<FaceDetectorPlayer *>( opaque );


    that->m_image_queue_lock.lock();

    if ( that->m_image_queue.empty() )
    {
        std::cerr << "image queue is empty" << std::endl;
        return nullptr;
    }

    //pop an image from our pool and give it to VLC
    cv::Mat *mat = that->m_image_queue.back();
    assert( mat );
    that->m_image_queue.pop_back();

    //we need to tell VLC where it can write
    //here, we only have one plane, data being packed as BGRA
    planes[0] = mat->data;

    //mat will be return as the picture parameters of unlock and display callback
    return mat;
}

//this callback is called after the image is decoded
void FaceDetectorPlayer::on_video_unlock( void *opaque, void *picture, void *const *planes )
{
    assert( opaque );
    FaceDetectorPlayer *that = reinterpret_cast<FaceDetectorPlayer *>( opaque );
    cv::Mat *img = reinterpret_cast<cv::Mat *>( picture );
    if ( img == nullptr )
    {
        std::cerr << "image is empty " << std::endl;
        that->m_image_queue_lock.unlock();
        return;
    }

    cv::cvtColor( *img, that->m_gray_image, CV_BGRA2GRAY );
    std::vector<cv::Rect> facelist;
    that->face_cascade.detectMultiScale( that->m_gray_image, facelist, 1.3, 5 );
    for ( cv::Rect &face : facelist )
    {
        cv::rectangle( *img, face, cv::Scalar( 255, 0, 255, 0 ) );
    }

    that->m_image_queue_lock.unlock();
}


//this callback is called when the image should be displayed
void FaceDetectorPlayer::on_video_display( void *opaque, void *picture )
{
    assert( opaque );
    FaceDetectorPlayer *that = reinterpret_cast<FaceDetectorPlayer *>( opaque );
    cv::Mat *mat = reinterpret_cast<cv::Mat *>( picture );

    if ( mat == nullptr )
        return;

    //display our image using opencv functions
    cv::imshow( "video", *mat );
    int code = cv::waitKey( 1 );

    //enqueue our buffer for future use
    {
        std::unique_lock<std::mutex>  queue_lock( that->m_image_queue_lock );
        that->m_image_queue.push_back( mat );
    }

    //check if the user press the Q key
    if ( code == 'q' )
    {
        //media player can't be stoped from its own thread, so we notify the main thread to do it
        that->notify_action( PlayerAction::STOP );
    }
}


// this callback is called to setup the desired format to render video
// The default values are provided as argument and can be overwritten
unsigned int FaceDetectorPlayer::on_format_setup( void **opaque, char *chroma,
                                                  unsigned *width, unsigned *height,
                                                  unsigned   *pitches, unsigned   *lines )
{
    std::cout << "format setup"  << std::endl;
    assert( opaque );
    FaceDetectorPlayer *that = reinterpret_cast<FaceDetectorPlayer *>( *opaque );

    std::unique_lock<std::mutex>  queue_lock( that->m_image_queue_lock );

    //we will change the format to BGRA
    memcpy( chroma, "BGRA", 4 );
    //and reduce the video size to speed up OpenCV processing
    *width = *width / 2;
    *height = *height / 2;

    //we pre-allocated images buffers
    //Here we use OpenCV mat
    for ( int i = 0; i < NB_ALLOCATED_PICTURE; ++i )
    {
        cv::Mat *mat = new cv::Mat( int( *height ), int( *width ), CV_8UC4 );
        that->m_image_queue.push_back( mat );
    }

    cv::Mat *mat = that->m_image_queue.front();
    assert( mat );

    //we need to set for each plane, the pitch and the number of lines
    //Here we only have one plane, each pixel is four octets B,G,R,A
    //so our pich is 4 * width,
    pitches[0] = mat->step;
    lines[0] = mat->rows;

    //we must return the number of allocated pictures
    return NB_ALLOCATED_PICTURE;
}

//this callback is called to release the resources allocated by on_format_setup
void FaceDetectorPlayer::on_format_cleanup( void  *opaque )
{
    assert( opaque );
    FaceDetectorPlayer *that = reinterpret_cast<FaceDetectorPlayer *>( opaque );
    std::unique_lock<std::mutex>  queue_lock( that->m_image_queue_lock );
    for ( cv::Mat *mat : that->m_image_queue )
    {
        delete mat;
    }
    that->m_image_queue.clear();
}

void FaceDetectorPlayer::notify_action( PlayerAction action )
{
    //we can't stop the player from its own thread, defer the action to the main tread
    std::unique_lock<std::mutex> lock( m_action_mutex );
    m_action = action;
    m_playcond.notify_all();
}

int main( int argc, char *argv[] )
{
    if ( argc < 2 )
    {
        std::cerr << "Usage: " << argv[0] << " [INPUT_FILE]" << std::endl;
        return 1;
    }

    //Initialise libvlc
    static const char *vlcArguments[] =
    {
        "--intf=dummy",
        "--ignore-config",
        "--no-media-library",
        "--no-one-instance",
        "--no-osd",
        "--no-snapshot-preview",
        "--no-stats",
        "--no-video-title-show",
    };

    libvlc_instance_t *vlcInstance = libvlc_new( sizeof( vlcArguments ) / sizeof( vlcArguments[0] ), vlcArguments );
    if ( vlcInstance ==  nullptr )
    {
        std::cerr << "Unable to initialise libvlc" << std::endl;
        return 1;
    }

    int retcode = 1;
    try
    {
        FaceDetectorPlayer player( vlcInstance, argv[1] );
        retcode = player.play();
    }
    catch ( std::exception &err )
    {
        std::cerr << err.what() << std::endl;
    }

    //release our resources
    libvlc_release( vlcInstance );
    return retcode;
}
