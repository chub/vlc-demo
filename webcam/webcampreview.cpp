#include "webcampreview.h"

#include "videofiltersmodel.h"

#include <QtCore/QDir>
#include <QApplication>
#include <QDesktopWidget>

static const char* vlcArguments[] = {
    "--intf=dummy",
    "--ignore-config",
    "--no-media-library",
    "--no-one-instance",
    "--no-osd",
    "--no-snapshot-preview",
    "--no-stats",
    "--no-video-title-show",
    "-vvv"
};

WebcamPreview::WebcamPreview(QWidget* parent)
    : QWidget(parent)
    , m_vlcInstance(libvlc_new(sizeof(vlcArguments) / sizeof(vlcArguments[0]), vlcArguments))
    , m_vlcMediaPlayer(0)
    , m_recording(false)
    , m_videoFiltersModel(new VideoFiltersModel(m_vlcInstance, this))
{
    setAutoFillBackground(true);

    QPalette palette = this->palette();
    palette.setColor(QPalette::Window, Qt::black);
    setPalette(palette);

    connect(m_videoFiltersModel, SIGNAL(enabledFiltersChanged()), this, SLOT(enabledFiltersChanged()));
}

WebcamPreview::~WebcamPreview()
{
    libvlc_release(m_vlcInstance);
}

void WebcamPreview::start()
{
    Q_ASSERT(!m_vlcMediaPlayer);

    const char* webcamMrl;
#if defined(Q_OS_LINUX)
    webcamMrl = "v4l2://";
#elif defined(Q_OS_WIN)
    webcamMrl = "dshow://";
#elif defined(Q_OS_MAC)
    webcamMrl = "qtcapture://";
#else
#error "unsupported platform"
#endif

    libvlc_media_t* vlcMedia = libvlc_media_new_location(m_vlcInstance, webcamMrl);
    if (m_recording) {
        Q_ASSERT(!m_recordingFilePath.isEmpty());

        // FIXME: Some of these parameters could be configurable.
        const char* recordingOptionPattern =
                "sout=#duplicate{"
                    "dst=display,"
                    "dst='transcode{vcodec=theo,vb=1800,acodec=vorb,ab=128}:standard{access=file,dst=%1}'"
                "}";

        QString recordingOption = QString(recordingOptionPattern).arg(m_recordingFilePath);
        libvlc_media_add_option(vlcMedia, recordingOption.toUtf8().constData());
        libvlc_media_add_option(vlcMedia, "v4l2-caching=100");
    }

    QStringList enabledFilters = m_videoFiltersModel->enabledFilters();
    if (!enabledFilters.isEmpty()) {
        QString filtersList = enabledFilters.join(":");

        libvlc_media_add_option(vlcMedia, QString(":video-filter=%1").arg(filtersList).toUtf8().constData());
        if (m_recording)
            libvlc_media_add_option(vlcMedia, QString(":sout-transcode-vfilter=%1").arg(filtersList).toUtf8().constData());
    }

    m_vlcMediaPlayer = libvlc_media_player_new_from_media(vlcMedia);
    libvlc_media_release(vlcMedia);

#if defined(Q_OS_LINUX)
    libvlc_media_player_set_xwindow(m_vlcMediaPlayer, winId());
#elif defined(Q_OS_MAC)
    libvlc_media_player_set_nsobject(m_vlcMediaPlayer, winId());
#elif defined(Q_OS_WIN)
    libvlc_media_player_set_hwnd(m_vlcMediaPlayer, winId());
#else
#error "unsupported platform"
#endif
    libvlc_media_player_play(m_vlcMediaPlayer);
}

void WebcamPreview::stop()
{
    Q_ASSERT(m_vlcMediaPlayer);

    libvlc_media_player_stop(m_vlcMediaPlayer);
    libvlc_media_player_release(m_vlcMediaPlayer);
    m_vlcMediaPlayer = 0;
}

void WebcamPreview::takeSnapshot(const QString& filePath)
{
    Q_ASSERT(m_vlcMediaPlayer);

    libvlc_video_take_snapshot(m_vlcMediaPlayer, 0, filePath.toUtf8().constData(), 0, 0);
}

void WebcamPreview::startRecording(const QString& filePath)
{
    Q_ASSERT(m_vlcMediaPlayer);
    Q_ASSERT(!m_recording);

    m_recording = true;
    m_recordingFilePath = filePath;
    stop();
    start();
}

void WebcamPreview::stopRecording()
{
    Q_ASSERT(m_vlcMediaPlayer);
    Q_ASSERT(m_recording);

    m_recording = false;
    m_recordingFilePath.clear();
    stop();
    start();
}

void WebcamPreview::closeEvent(QCloseEvent* event)
{
    stop();
    QWidget::closeEvent(event);
}

QSize WebcamPreview::sizeHint() const
{
    // FIXME: The size hint should probably change once we know the video size.
    return QSize(400, 300);
}

void WebcamPreview::enabledFiltersChanged()
{
    if (m_vlcMediaPlayer) {
        stop();
        start();
    }
}
