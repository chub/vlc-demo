#include "mainwindow.h"

#include "webcampreview.h"

#include <QApplication>

int main(int argc, char** argv)
{
    QCoreApplication::setApplicationName("Seer");
    QCoreApplication::setOrganizationName("VideoLAN");

    QApplication app(argc, argv);

    MainWindow mainWindow;
    mainWindow.show();
    mainWindow.webcamPreview()->start();

    return app.exec();
}
