TARGET = seer
CONFIG += qt link_pkgconfig
PKGCONFIG += libvlc
QT += xml gui widgets core

HEADERS += \
    mainwindow.h \
    webcampreview.h \
    videofiltersmodel.h \
    settingsdialog.h

SOURCES += \
    mainwindow.cpp \
    webcampreview.cpp \
    main.cpp \
    videofiltersmodel.cpp \
    settingsdialog.cpp
