#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>

#include <QtCore/QDir>

class QButtonGroup;
class QListView;
class QPushButton;
class QTimer;
class VideoFiltersModel;
class WebcamPreview;
class MainWindow : public QMainWindow
{
    Q_OBJECT

private:
    enum Mode {
        TakePhotoMode,
        TakeMultiplePhotosMode,
        RecordVideoMode
    };

public:
    MainWindow();
    ~MainWindow();

    WebcamPreview* webcamPreview() const { return m_webcamPreview; }

private:
    void setupInterface();
    QDir dataDirectory() const;
    QString suggestedFileName() const;

private slots:
    void takePhoto();
    void startStopPhotoBatch(bool start);
    void startStopRecording(bool start);
    void showHideEffects(bool show);
    void showSettingsDialog();

private:
    Mode m_mode;

    WebcamPreview* m_webcamPreview;
    QListView* m_videoFiltersView;

    QTimer* m_photoBatchTimer;
};

#endif // MAINWINDOW_H
