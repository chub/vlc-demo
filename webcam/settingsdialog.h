#ifndef SETTINGSDIALOG_H
#define SETTINGSDIALOG_H

#include <QDialog>

class QLineEdit;
class QSpinBox;
class SettingsDialog : public QDialog
{
    Q_OBJECT

public:
    SettingsDialog(QWidget* parent = 0);
    ~SettingsDialog();

public slots:
    void accept();

private:
    void setupInterface();
    void load();
    void save();

private:
    QLineEdit* m_dataDirectoryLineEdit;
    QSpinBox* m_photoBatchIntervalSpinBox;
};

#endif // SETTINGSDIALOG_H
