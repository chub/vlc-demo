#include "mainwindow.h"

#include "settingsdialog.h"
#include "videofiltersmodel.h"
#include "webcampreview.h"

#include <QtCore/QDateTime>
#include <QtCore/QSettings>
#include <QtCore/QTimer>
#include <QAction>
#include <QButtonGroup>
#include <QGridLayout>
#include <QHBoxLayout>
#include <QListView>
#include <QPushButton>
#include <QToolBar>

MainWindow::MainWindow()
    : QMainWindow()
    , m_mode(TakePhotoMode)
    , m_webcamPreview(new WebcamPreview(this))
    , m_videoFiltersView(0)
    , m_photoBatchTimer(new QTimer(this))
{
    m_photoBatchTimer->setSingleShot(false);
    connect(m_photoBatchTimer, SIGNAL(timeout()), this, SLOT(takePhoto()));

    setupInterface();
}

MainWindow::~MainWindow()
{
}

void MainWindow::setupInterface()
{
    QWidget* mainWidget = new QWidget();
    QVBoxLayout* mainLayout = new QVBoxLayout();
    mainWidget->setLayout(mainLayout);
    setCentralWidget(mainWidget);

    QWidget* view = new QWidget();
    QHBoxLayout* viewLayout = new QHBoxLayout();
    view->setLayout(viewLayout);
    mainLayout->addWidget(view);

    m_webcamPreview->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Expanding);
    viewLayout->addWidget(m_webcamPreview);

    m_videoFiltersView = new QListView();
    m_videoFiltersView->setHidden(true);
    m_videoFiltersView->setSizePolicy(QSizePolicy::Maximum, QSizePolicy::Preferred);
    m_videoFiltersView->setModel(m_webcamPreview->videoFiltersModel());
    viewLayout->addWidget(m_videoFiltersView);

    QWidget* controlWidget = new QWidget();
    controlWidget->setSizePolicy(QSizePolicy::Preferred, QSizePolicy::Fixed);
    QHBoxLayout* controlLayout = new QHBoxLayout();
    controlWidget->setLayout(controlLayout);
    mainLayout->addWidget(controlWidget);

    controlLayout->addStretch(1);

    QPushButton* takePhotoButton = new QPushButton(QIcon::fromTheme("camera-photo"), tr("Take a photo"));
    connect(takePhotoButton, SIGNAL(clicked()), this, SLOT(takePhoto()));
    controlLayout->addWidget(takePhotoButton);

    QPushButton* takePhotoBatchButton = new QPushButton(QIcon::fromTheme("camera-photo"), tr("Take multiple photos"));
    takePhotoBatchButton->setCheckable(true);
    connect(takePhotoBatchButton, SIGNAL(toggled(bool)), this, SLOT(startStopPhotoBatch(bool)));
    controlLayout->addWidget(takePhotoBatchButton);

    QPushButton* recordVideoButton = new QPushButton(QIcon::fromTheme("media-record"), tr("Record a video"));
    recordVideoButton->setCheckable(true);
    connect(recordVideoButton, SIGNAL(toggled(bool)), this, SLOT(startStopRecording(bool)));
    controlLayout->addWidget(recordVideoButton);

    controlLayout->addStretch(1);

    QToolBar* toolBar = new QToolBar(this);
    toolBar->setToolButtonStyle(Qt::ToolButtonTextBesideIcon);
    addToolBar(toolBar);

    QAction* effectsAction = toolBar->addAction(QIcon::fromTheme("video"), tr("Effects"), this, SLOT(showHideEffects(bool)));
    effectsAction->setCheckable(true);

    toolBar->addAction(QIcon::fromTheme("camera-web"), tr("Settings"), this, SLOT(showSettingsDialog()));
}

QDir MainWindow::dataDirectory() const
{
    QSettings settings;
    QString directory = settings.value("dataDirectory", QDir::homePath()).toString();
    QDir dir(directory);
    dir.mkpath("./");
    return dir;
}

QString MainWindow::suggestedFileName() const
{
    return QDateTime::currentDateTime().toString("yyyy_MM_dd_hh_mm_ss");
}

void MainWindow::takePhoto()
{
    m_webcamPreview->takeSnapshot(dataDirectory().absoluteFilePath(suggestedFileName() + ".png"));
}

void MainWindow::startStopPhotoBatch(bool start)
{
    if (start) {
        QSettings settings;
        int interval = settings.value("photoBatchInterval", 1000).toInt();
        if (interval < 1000)
            interval = 1000;
        m_photoBatchTimer->setInterval(interval);

        m_photoBatchTimer->start();
    } else {
        m_photoBatchTimer->stop();
    }
}

void MainWindow::startStopRecording(bool start)
{
    if (start)
        m_webcamPreview->startRecording(dataDirectory().absoluteFilePath(suggestedFileName() + ".ogv"));
    else
        m_webcamPreview->stopRecording();
}

void MainWindow::showHideEffects(bool show)
{
    m_videoFiltersView->setVisible(show);
}

void MainWindow::showSettingsDialog()
{
    SettingsDialog* dialog = new SettingsDialog(this);
    dialog->setAttribute(Qt::WA_DeleteOnClose);
    dialog->show();
}
