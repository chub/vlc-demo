#include <vlc/vlc.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <pthread.h>

/**
 * This structure will holds a context we can carry around
 */
typedef struct
{
    libvlc_media_t *p_media;
    pthread_mutex_t mutex;
    pthread_cond_t cond;
    int i_ret_code;

} context_t;


/**
 * @brief print_meta will print, when available, the metadata @a e_meta from media @a p_m
 * @param p_m the media.
 * @param psz_label a string label to be diplayed.
 * @param e_meta the metadata key.
 */
void print_meta( libvlc_media_t *p_m, const char *psz_label, libvlc_meta_t e_meta )
{
    char *psz_meta = libvlc_media_get_meta( p_m, e_meta );
    if ( psz_meta )
    {
        printf( "  %s: %s\n", psz_label, psz_meta );
        free( psz_meta );
    }
}


/**
 * @brief print_all_meta will print every available meta data from the media @a p_m
 * @param p_m the media.
 */
void print_all_meta( libvlc_media_t *p_m )
{
    printf( "Metadata\n" );
    // Read all tags from the media.
    print_meta( p_m, "title", libvlc_meta_Title );
    print_meta( p_m, "artist", libvlc_meta_Artist );
    print_meta( p_m, "genre", libvlc_meta_Genre );
    print_meta( p_m, "copyright", libvlc_meta_Copyright );
    print_meta( p_m, "album", libvlc_meta_Album );
    print_meta( p_m, "tracknumber", libvlc_meta_TrackNumber );
    print_meta( p_m, "description", libvlc_meta_Description );
    print_meta( p_m, "rating", libvlc_meta_Rating );
    print_meta( p_m, "date", libvlc_meta_Date );
    print_meta( p_m, "setting", libvlc_meta_Setting );
    print_meta( p_m, "url", libvlc_meta_URL );
    print_meta( p_m, "language", libvlc_meta_Language );
    print_meta( p_m, "now playing", libvlc_meta_NowPlaying );
    print_meta( p_m, "publisher", libvlc_meta_Publisher );
    print_meta( p_m, "encoded by", libvlc_meta_EncodedBy );
    print_meta( p_m, "artwork URL", libvlc_meta_ArtworkURL );
    print_meta( p_m, "track id", libvlc_meta_TrackID );
    print_meta( p_m, "track total", libvlc_meta_TrackTotal );
    print_meta( p_m, "director", libvlc_meta_Director );
    print_meta( p_m, "season", libvlc_meta_Season );
    print_meta( p_m, "episode", libvlc_meta_Episode );
    print_meta( p_m, "showname", libvlc_meta_ShowName );
    print_meta( p_m, "actors", libvlc_meta_Actors );
    print_meta( p_m, "album artist)", libvlc_meta_AlbumArtist );
    print_meta( p_m, "disc number", libvlc_meta_DiscNumber );
    print_meta( p_m, "disc total", libvlc_meta_DiscTotal );
}

/**
 * @brief print_track_info print informations about a single track from a media
 * @param p_track a pointer to the track
 */
void print_track_info( const libvlc_media_track_t *p_track )
{
    //show informations according to the type of the track
    switch ( p_track->i_type )
    {
        case libvlc_track_audio:
            printf( "  Stream #%i(%s): Audio: %s, %i channels, %i Hz\n",
                    p_track->i_id,
                    p_track->psz_language ? p_track->psz_language : "und",
                    libvlc_media_get_codec_description( p_track->i_type, p_track->i_codec ),
                    p_track->audio->i_channels,
                    p_track->audio->i_rate
                  );
            break;
        case libvlc_track_video:
            printf( "  Stream #%i(%s): Video: %s, %ix%i, SAR %i:%i, %.2f fps\n",
                    p_track->i_id,
                    p_track->psz_language ? p_track->psz_language : "und",
                    libvlc_media_get_codec_description( p_track->i_type, p_track->i_codec ),
                    p_track->video->i_width, p_track->video->i_height,
                    p_track->video->i_sar_num, p_track->video->i_sar_den,
                    (float)(p_track->video->i_frame_rate_num) / (float)(p_track->video->i_frame_rate_den)
                  );
            break;
        case libvlc_track_text:
            printf( "  Subtitle #%i(%s): %s, %s\n",
                    p_track->i_id,
                    p_track->psz_language ? p_track->psz_language : "und",
                    libvlc_media_get_codec_description( p_track->i_type, p_track->i_codec ),
                    p_track->subtitle->psz_encoding
                  );
            break;
        case libvlc_track_unknown:
        default:
            printf( "  Stream #%i: Unknowned\n", p_track->i_id );
            break;
    }
}

/**
 * @brief print_media_info
 * @param p_media
 */
void print_media_info( libvlc_media_t *p_media )
{
    //print media metadata
    print_all_meta( p_media );

    //print media resource location
    char *psz_mrl = libvlc_media_get_mrl( p_media );
    printf( "Input %s\n", psz_mrl );
    free( psz_mrl );

    time_t duration = libvlc_media_get_duration( p_media );
    printf( "Duration: %02li:%02li:%02li.%03li\n",
            duration / 3600000,
            ( duration / 60000 ) % 60,
            ( duration / 1000 ) % 60,
            duration  % 1000
          );

    //show information about each tracks
    libvlc_media_track_t  **pp_tracks_list = NULL;
    int nb_tracks = libvlc_media_tracks_get( p_media, &pp_tracks_list );
    for ( int i = 0; i < nb_tracks; ++i )
    {
        libvlc_media_track_t *p_track = pp_tracks_list[i];
        print_track_info( p_track );
    }

    libvlc_media_tracks_release( pp_tracks_list, nb_tracks );
}


/**
 * @brief on_media_parsed
 * @param p_event
 * @param p_data
 */
void on_media_parsed( const struct libvlc_event_t *p_event, void *p_data )
{
    //we are only interested by MediaParsedChanged events
    if ( p_event->type != libvlc_MediaParsedChanged )
        return;

    context_t *p_ctx = ( context_t * ) p_data;
    switch ( p_event->u.media_parsed_changed.new_status )
    {
        case libvlc_media_parsed_status_skipped:
            fprintf( stderr, "unable to parse this kind of media\n" );
            break;
        case libvlc_media_parsed_status_failed:
            fprintf( stderr, "failed to parse media\n" );
            break;
        case libvlc_media_parsed_status_timeout:
            fprintf( stderr, "parsing media timed out\n" );
            break;
        case libvlc_media_parsed_status_done:
            print_media_info( p_ctx->p_media );
            p_ctx->i_ret_code = 0;
            break;
    }

    //notify that the program can exit
    pthread_mutex_lock ( &p_ctx->mutex );
    pthread_cond_signal ( &p_ctx->cond );
    pthread_mutex_unlock ( &p_ctx->mutex );
}


int main( int argc, char *argv[] )
{
    if ( argc < 2 )
    {
        fprintf( stderr, "Usage: %s [INPUT_FILE]\n", argv[0] );
        return 1;
    }


    /**
     * vlc initialisation arguments
     */
    static const char *vlcArguments[] =
    {
        "--intf=dummy",
        "--ignore-config",
        "--no-media-library",
        "--no-one-instance",
        "--no-osd",
        "--no-snapshot-preview",
        "--no-stats",
        "--no-video-title-show"
    };

    libvlc_instance_t *vlcInstance = libvlc_new( sizeof( vlcArguments ) / sizeof( vlcArguments[0] ), vlcArguments );

    //print generic information about libvlc
    printf( "%s %s\n", argv[0], libvlc_get_version() );
    printf( "  built with %s\n", libvlc_get_compiler() );

    context_t ctx;
    ctx.i_ret_code = 1;
    pthread_mutex_init( &ctx.mutex, NULL );
    pthread_cond_init( &ctx.cond, NULL );

    //create a media to our input file
    ctx.p_media = libvlc_media_new_path( vlcInstance, argv[1] );
    if ( ctx.p_media == NULL )
    {
        fprintf( stderr, "Invalid media %s\n", argv[1] );
        goto end;
    }

    //register a callback to be notified when parsing is done
    //we need to register our callback to the event manager of our media.
    if ( libvlc_event_attach(
             libvlc_media_event_manager( ctx.p_media ),
             libvlc_MediaParsedChanged,
             on_media_parsed, //our callback
             &ctx
         ) != 0 )
    {
        fprintf( stderr, "error while registering event handler" );
        goto end;
    }

    //parse the media, this is done asynchronously
    if ( libvlc_media_parse_with_options(
             ctx.p_media,
             libvlc_media_parse_local,
             5000 ) != 0 )
    {
        fprintf( stderr, "error parsing media" );
        goto end;
    }

    //we wait for the program to end
    pthread_mutex_lock( &ctx.mutex );
    pthread_cond_wait( &ctx.cond, &ctx.mutex );
    pthread_mutex_unlock( &ctx.mutex );

end:
    //release our resources
    libvlc_media_release( ctx.p_media );
    libvlc_release( vlcInstance );
    return ctx.i_ret_code;
}
