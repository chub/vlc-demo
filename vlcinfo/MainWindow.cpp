#include "MainWindow.h"

MainWindow::MainWindow()
{
    if(VLC::init() != 0)                           // Initialize libVLC and check for error.
    {
        QMessageBox::warning(this, tr("Error"),    // Show error message and quit the program.
                             tr("Error initializing libVLC:\n%1").arg(libvlc_errmsg()),
                             QMessageBox::Ok);
        exit(0);
    }
    m_mainwidget = new QWidget;
    m_mainlayout = new QVBoxLayout(m_mainwidget);
    m_buttonlayout = new QHBoxLayout();
    m_openbutton = new QPushButton(tr("Open File"));           // Create button to open a new file.
    m_opendirbutton = new QPushButton(tr("Open Directory"));   // Create button to open directory.
    m_savebutton = new QPushButton(tr("Save"));                // Create button to save information to file.
    m_quitbutton = new QPushButton(tr("Quit"));                // Create button that quits the application.
    m_stackw = new QStackedWidget();                           // Widget switching between start-screen and tabs.
    m_mediatabs = new TabWidget(this);                         // Create the mainwidget that contains the tabs with information.
    m_startw = new StartWidget(this);                          // Create startwidget.
    m_openbutton->hide();                                    // Buttons are hidden when there are no open tabs.
    m_opendirbutton->hide();
    m_savebutton->hide();
    m_stackw->addWidget(m_startw);                           // Add widgets to stack.
    m_stackw->addWidget(m_mediatabs);
    m_buttonlayout->addWidget(m_openbutton);                 // Add buttons to layout.
    m_buttonlayout->addWidget(m_opendirbutton);
    m_buttonlayout->addWidget(m_savebutton);
    m_buttonlayout->addStretch();
    m_buttonlayout->addWidget(m_quitbutton);
    m_mainlayout->addWidget(m_stackw);                       // Add buttons and widget-stack to mainlayout.
    m_mainlayout->addLayout(m_buttonlayout);
    setCentralWidget(m_mainwidget);                            // Use as central widget in the mainwindow.
    setWindowTitle("VLCinfo");
    resize(600, 400);                                          // Resize the maindindow to appropriate size.
    connect(m_openbutton, SIGNAL(clicked()),                   // Create connections for buttons.
            this, SLOT(loadFile()));
    connect(m_opendirbutton, SIGNAL(clicked()),
            this, SLOT(loadDir()));
    connect(m_quitbutton, SIGNAL(clicked()),
            qApp, SLOT(quit()));
    connect(m_savebutton, SIGNAL(clicked()),
            m_mediatabs, SLOT(saveToFile()));
}


void MainWindow::loadFile()                   // Choose a one or more files and show information about it.
{
    QStringList paths = QFileDialog::getOpenFileNames(this, tr("Choose media files to load"));
    foreach(QString path, paths)
        m_mediatabs->addMedia(path);
}


void MainWindow::loadFile(char *filename)      // Show information about the file with the given name.
{
    m_mediatabs->addMedia(QString::fromUtf8(filename));
}


void MainWindow::loadDir()                    // Choose directory and show information about all files in it.
{
    QString dirpath = QFileDialog::getExistingDirectory(this, tr("Choose directory"));
    QStringList paths = QDir(dirpath).entryList(QDir::Files);
    foreach(QString path, paths)
        m_mediatabs->addMedia(dirpath + "/" + path);
}


void MainWindow::showStartWidget()                  // Shows the start widget.
{
    m_stackw->setCurrentIndex(0);
    m_openbutton->hide();
    m_opendirbutton->hide();
    m_savebutton->hide();
}


void MainWindow::hideStartWidget()                  // Hide start widget.
{
    m_stackw->setCurrentIndex(1);
    m_openbutton->show();
    m_opendirbutton->show();
    m_savebutton->show();
}
