#include "MainWidgets.h"

StartWidget::StartWidget(QWidget *parent) : QWidget(parent)   // Create child-widgets.
{
    m_mainlayout = new QVBoxLayout(this);
    m_buttonlayout = new QHBoxLayout();
    m_openbutton = new QPushButton(tr("Open File"));          // Create buttons to open file and directory.
    m_opendirbutton = new QPushButton(tr("Open Directory"));
    m_iconlabel = new QLabel(tr("<center>"
                                "<table border=0>"
                                "<tr>"
                                "<td align=\"center\"><img src=\"%1\"></td>"
                                "</tr>"
                                "<tr>"
                                "<td align=\"center\"><b><font size=\"50\">VLCinfo</font></b></td>"
                                "</tr>"
                                "</table>"
                                "</center>").arg(iconpath));   // Create label with icon.
    m_buttonlayout->addStretch();                           // Add buttons to layout.
    m_buttonlayout->addWidget(m_openbutton);
    m_buttonlayout->addWidget(m_opendirbutton);
    m_buttonlayout->addStretch();
    m_mainlayout->addStretch();
    m_mainlayout->addWidget(m_iconlabel);                   // Add buttons and icon to layout.
    m_mainlayout->addLayout(m_buttonlayout);
    m_mainlayout->addStretch();
    connect(m_openbutton, SIGNAL(clicked()),
            parent, SLOT(loadFile()));
    connect(m_opendirbutton, SIGNAL(clicked()),
            parent, SLOT(loadDir()));
}


TabWidget::TabWidget(QWidget *parent) : QTabWidget(parent)
{
    m_shortcut = new QShortcut(tr("Ctrl+w"), this, SLOT(removeMedia()));   // Set shortcut for closing tab.
    setTabsClosable(true);                                                 // Make tabs closable.
    connect(this, SIGNAL(tabCloseRequested(int)),                          // Remove tab when it is closed.
            this, SLOT(removeMedia(int)));
    connect(this, SIGNAL(firstTabOpened()),                                // Show tabwidget if a tab is available.
            parent, SLOT(hideStartWidget()));
    connect(this, SIGNAL(lastTabClosed()),                                 // Show startwidget if last tab was closed.
            parent, SLOT(showStartWidget()));
}


TabWidget::~TabWidget()                                           // Clean up
{
    delete m_shortcut;
    qDeleteAll(m_mediafiles);
}


void TabWidget::addMedia(const QString& filename)             // Add a media to the widget.
{
    int index = indexOf(filename);                            // Check if media is already loaded.
    if(index != -1)                                           // Already loaded.
        setCurrentIndex(index);                               // Show information about the requested file.
    else
    {
        if(!QFile(filename).exists())
        {
            QMessageBox::critical(this, tr("Error"), tr("File %1 does not exist.").arg(filename));
            return;
        }
        Media *m = new Media(filename);                       // Create media-object for the file.
        if(!m->isValid())                                     // Check if media is valid.
        {
            QMessageBox::critical(this, tr("Error"), tr("Media %1 is invalid.").arg(filename));
            return;
        }
        MediaWidget *w = new MediaWidget(m, this);            // Create widget for the file.
        m_mediafiles.push_back(m);                            // Add to the list of files.
        m_mediawidgets.push_back(w);                          // Add to the list of widgets.
        QString tabtext, name = QFileInfo(filename).fileName();
        if(name.length() > 20)
            tabtext = name.left(17) + "...";
        else
            tabtext = name.left(20);
        int index = addTab(w, tabtext);                       // Add a tab with the new information;
        setCurrentIndex(index);
        setTabToolTip(index, name);
        if(count() == 1)                                      // Check if it was the first opened tab.
            emit firstTabOpened();
    }
}


void TabWidget::removeMedia()                                 // Remove current media from the mainwindow.
{
    removeMedia(currentIndex());
}


void TabWidget::removeMedia(int index)                        // Remove tab with the passed index.
{
    removeTab(index);                                         // Remove the widget.
    delete m_mediafiles.takeAt(index);                        // Remove media from the list.
    delete m_mediawidgets.takeAt(index);                      // Remove widget from the list.
    if(count() == 0)                                          // Check if last tab was closed.
        emit lastTabClosed();
}


void TabWidget::saveToFile()                                  // Saves information of current media to file.
{
    // Get a filename and try to create the file.
    QString filepath = QFileDialog::getSaveFileName(this, tr("Choose filename"));
    if(filepath.isEmpty())
        return;
    QFile f(filepath);
    if(!f.open(QIODevice::WriteOnly | QIODevice::Text))
    {
        QMessageBox::critical(NULL, tr("Error"), tr("Error saving information to file:\n%1").arg(f.errorString()));
        return;
    }
    m_mediafiles[currentIndex()]->saveToFile(f);              // Get the information and write it to the file.
}


int TabWidget::indexOf(const QString& filename)               // Return the tab-index of the file with the passed name.
{
    for(int i = 0; i < m_mediafiles.size(); i++)              // Go through all media-files, compare filenames and return index if found, -1 otherwise.
        if(m_mediafiles[i]->filename() == filename)
            return i;
    return -1;
}
