#include "TrackInfo.h"

TrackType::TrackType(const libvlc_track_type_t type)            // libVLC-type is passed as parameter.
{
    switch(type)                                                // Get type as a string.
    {
        case libvlc_track_audio:
        {
            m_stringtype = "Audio";
            break;
        }
        case libvlc_track_video:
        {
            m_stringtype = "Video";
            break;
        }
        case libvlc_track_text:
        {
            m_stringtype = "Text";
            break;
        }
        default:
        {
            m_stringtype = "Unknown";
            break;
        }
    }
}


TrackInfo::TrackInfo(const libvlc_media_track_info_t info) : m_trackinfo(info),     // Track-information is passed as parameter.
m_type(info.i_type)
{
    char codec[5];
    memcpy(codec, &m_trackinfo.i_codec, 4);
    codec[4] = '\0';
    m_codecstring = codec;
}


QPair<unsigned int, unsigned int> TrackInfo::typeDependentInfo() const
{
    if(m_trackinfo.i_type == libvlc_track_audio)
        return QPair<unsigned int, unsigned int>(m_trackinfo.u.audio.i_channels, m_trackinfo.u.audio.i_rate);
    else
        return QPair<unsigned int, unsigned int>(m_trackinfo.u.video.i_width, m_trackinfo.u.video.i_height);
}
