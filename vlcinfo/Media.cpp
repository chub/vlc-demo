#include "Media.h"

QMap<QString, QString> ContainerType::sm_typemap;

void ContainerType::init()                 // Initialize map with according values.
{
    sm_typemap["aiff"] = "AIFF";
    sm_typemap["aif"] =  "AIFF";
    sm_typemap["aifc"] = "AIFF";
    sm_typemap["wav"] = "WAV";
    sm_typemap["xmf"] = "XMF";
    sm_typemap["fits"] = "FITS";
    sm_typemap["tif"] = "Tagged Image File Format";
    sm_typemap["tiff"] = "Tagged Image File Format";
    sm_typemap["3gp"] = "3GP";
    sm_typemap["asf"] = "Advanced Systems Format";
    sm_typemap["wma"] = "Advanced Systems Format";
    sm_typemap["wmv"] = "Advanced Systems Format";
    sm_typemap["avi"] = "Audio Video Interleave";
    sm_typemap["dvr-ms"] = "Microsoft Digital Video Recording";
    sm_typemap["flv"] = "Flash Video";
    sm_typemap["f4v"] = "Flash Video";
    sm_typemap["f4p"] = "Flash Video";
    sm_typemap["f4a"] = "Flash Video";
    sm_typemap["f4b"] = "Flash Video";
    sm_typemap["iff"] = "Interchange File Format";
    sm_typemap["mkv"] = "Matroska";
    sm_typemap["mks"] = "Matroska";
    sm_typemap["mka"] = "Matroska";
    sm_typemap["jp2"] = "JPEG 2000";
    sm_typemap["j2k"] = "JPEG 2000";
    sm_typemap["jpf"] = "JPEG 2000";
    sm_typemap["jpx"] = "JPEG 2000";
    sm_typemap["jpm"] = "JPEG 2000";
    sm_typemap["mj2"] = "JPEG 2000";
    sm_typemap["mov"] = "QuickTime File Format";
    sm_typemap["qt"] = "QuickTime File Format";
    sm_typemap["mpg"] = "MPEG program stream";
    sm_typemap["mpeg"] = "MPEG program stream";
    sm_typemap["ps"] = "MPEG program stream";
    sm_typemap["ts"] = "MPEG transport stream";
    sm_typemap["mp4"] = "MPEG-4 Part 14";
    sm_typemap["ogv"] = "Ogg";
    sm_typemap["oga"] = "Ogg";
    sm_typemap["ogx"] = "Ogg";
    sm_typemap["ogg"] = "Ogg";
    sm_typemap["spx"] = "Ogg";
    sm_typemap["rm"] = "RealMedia";
}


QString ContainerType::typeOf(const QString& suffix)    // Return the type of the container as a string.
{
    QString type = sm_typemap[suffix];
    if(!type.isEmpty())                                 // Suffix is in the map.
        return type;
    return QObject::tr("Unknown");
}


MediaInfo::MediaInfo(libvlc_media_t *const m)                                     // Media is passed as parameter.
{
    libvlc_media_track_info_t *trackinfo;                                         // Array to store information about the tracks of the media.
    int numtracks = libvlc_media_get_tracks_info(m, &trackinfo);                  // Get information about the tracks.
    m_duration = libvlc_media_get_duration(m);                                    // Get duration of the track.
    // Calculate length of the media in the format: (h)h:mm:ss
    int hours = m_duration / 1000 / 60 / 60, minutes = m_duration / 1000 / 60 % 60, seconds = m_duration / 1000 % 60;
    m_durationstring = (hours > 0 ? QString::number(hours) + ":" : "") +
                       (minutes < 10 ? QString::number(0) + QString::number(minutes) : QString::number(minutes)) + ":" +
                       (seconds < 10 ? QString::number(0) + QString::number(seconds) : QString::number(seconds));
    for(int i = 0; i < numtracks; i++)                                            // Create information for each track.
        m_trackinfo.push_back(new TrackInfo(trackinfo[i]));
    delete[] trackinfo;                                                           // Delete track-info from libVLC, not needeed anymore.
}


MediaInfo::~MediaInfo()                                                         // Clean up
{
    qDeleteAll(m_trackinfo);
}


Media::Media(const QString& filename) : m_filename(filename)      // Filename of the media is passed.

{
    m_media = libvlc_media_new_path(VLC::instance(),              // Load media.
                                    m_filename.toStdString().c_str());
    if(m_media != NULL)                                           // Check if media is valid.F
    {
        libvlc_media_parse(m_media);                              // Parse media to read information from it.
        m_metainfo = new MetaInfo(m_media);                       // Read tags from it.
        m_mediainfo = new MediaInfo(m_media);                     // Read media-information from it.
    }
    if(!ContainerType::isInit())
        ContainerType::init();
    m_containertype = ContainerType::typeOf(QFileInfo(m_filename).suffix()); // Get container type from file suffix.
}


Media::~Media()                                                   // Clean up
{
    if(m_media != NULL)
    {
        libvlc_media_release(m_media);
        delete m_metainfo;
        delete m_mediainfo;
    }
}


bool Media::isValid() const                                      // Return true if media is valid.
{
    return m_media != NULL;
}


void Media::saveToFile(QFile& file) const                       // Write information to passed file.
{
    int numtracks = m_mediainfo->numTracks();
    const QList<TrackInfo *> tracks = m_mediainfo->trackInfo();
    QPair<unsigned int, unsigned int> typedep;
    QTextStream t(&file);
    QFileInfo info(m_filename);
    QString nl = QObject::tr("\n");

    t << m_filename << nl
      << QObject::tr("General Information") << nl
      << QObject::tr("\tFile Name: ") << info.fileName() << nl
      << QObject::tr("\tPath: ") << info.path() << nl
      << QObject::tr("\tContainer type: ") << m_containertype << nl
      << QObject::tr("\tSize: ") << (QString::number((qreal)info.size() / 1000000, 'g', 3) + " MB") << nl
      << QObject::tr("\tCreated:") << info.created().toString("dd/MM/yyyy hh:mm") << nl
      << QObject::tr("Tags") << nl;
    if(!m_metainfo->title().isEmpty())
        t << QObject::tr("\tTitle: ") << m_metainfo->title() << nl;
    if(!m_metainfo->album().isEmpty())
        t << QObject::tr("\tAlbum: ") << m_metainfo->album() << nl;
    if(!m_metainfo->artist().isEmpty())
        t << QObject::tr("\tArtist: ") << m_metainfo->artist() << nl;
    if(!m_metainfo->trackNumber().isEmpty())
        t << QObject::tr("\tTracknumber: ") << m_metainfo->trackNumber() << nl;
    if(!m_metainfo->date().isEmpty())
        t << QObject::tr("\tDate: ") << m_metainfo->date() << nl;
    if(!m_metainfo->genre().isEmpty())
        t << QObject::tr("\tGenre: ") << m_metainfo->genre() << nl;
    if(!m_metainfo->copyright().isEmpty())
        t << QObject::tr("\tCopyright: ") << m_metainfo->copyright() << nl;
    if(!m_metainfo->rating().isEmpty())
        t << QObject::tr("\tRating: ") << m_metainfo->rating() << nl;
    if(!m_metainfo->description().isEmpty())
        t << QObject::tr("\tDescription: ") << m_metainfo->description() << nl;
    if(!m_metainfo->language().isEmpty())
        t << QObject::tr("\tLanguage: ") << m_metainfo->language() << nl;
    if(!m_metainfo->setting().isEmpty())
        t << QObject::tr("\tSetting: ") << m_metainfo->setting() << nl;
    if(!m_metainfo->url().isEmpty())
        t << QObject::tr("\tURL: ") << m_metainfo->url() << nl;
    if(!m_metainfo->nowPlaying().isEmpty())
        t << QObject::tr("\tNow Playing: ") << m_metainfo->nowPlaying() << nl;
    if(!m_metainfo->publisher().isEmpty())
        t << QObject::tr("\tPublisher: ") << m_metainfo->publisher() << nl;
    if(!m_metainfo->encodedBy().isEmpty())
        t << QObject::tr("\tEncoded By: ") << m_metainfo->encodedBy() << nl;
    if(!m_metainfo->artworkUrl().isEmpty())
        t << QObject::tr("\tArtwork-URL: ") << m_metainfo->artworkUrl() << nl;
    if(!m_metainfo->trackId().isEmpty())
        t << QObject::tr("\tTrack-ID: ") << m_metainfo->trackId() << nl << nl;
    t << QObject::tr("Media Information\n");
    if(m_mediainfo->duration() > 0)
        t << QObject::tr("\tDuration: ") << m_mediainfo->durationString() << nl;

    for(int i = 0; i < numtracks; i++)
    {
        typedep = tracks[i]->typeDependentInfo();
        t << QObject::tr("\tTrack #") << i << nl
                << QObject::tr("\t\tType: ") << tracks[i]->type().toString() << nl
                << QObject::tr("\t\tCodec: ") << tracks[i]->codecString() << nl
                << QObject::tr("\t\tID: ") << tracks[i]->id() << nl
                << QObject::tr("\t\tProfile: ") << tracks[i]->profile() << nl
                << QObject::tr("\t\tLevel: ") << tracks[i]->level() << nl;
        if(tracks[i]->vlcType() == libvlc_track_audio)
        {
            t << QObject::tr("\t\tChannels: ") << typedep.first << nl
                    << QObject::tr("\t\tSample Rate: ") << typedep.second << QObject::tr(" Hz") << nl;
        }
        else if(tracks[i]->vlcType() == libvlc_track_video)
        {
            t << QObject::tr("\t\tWidth: ") << typedep.first << nl
                    << QObject::tr("\t\tHeight: ") << typedep.second << nl;
        }
    }
    t << nl;
}
