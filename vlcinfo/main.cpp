#include "Application.h"

// Create the application and start the event-loop.
int main(int argc, char *argv[])
{
    for(int i = 1; i < argc; i++)
    {
        if(strcmp("--no-gui", argv[i]) == 0 ||
           strcmp("-h", argv[i]) == 0 ||
           strcmp("--help", argv[i]) == 0)
        {
            Cli cli(argc, argv);
            return cli.exec();
        }
    }
    QApplication app (argc, argv);
    MainWindow mainwindow;
    for(int i = 1; i < argc; i++)                   // Assume that all parameters are file names and load them.
        mainwindow.loadFile(argv[i]);
    mainwindow.show();
    return app.exec();
}
