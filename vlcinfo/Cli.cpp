#include "Cli.h"

// Constructor evaluating command line parameters
Cli::Cli(int argc, char *argv[]) : m_usage(false),
                                   m_filename(""),
                                   m_format(OutputFormat::Default)
{
    if(VLC::init() != 0)                           // Initialize libVLC and check for error.
    {
        std::cout << QObject::tr("Error initializing libVLC:\n%1").arg(libvlc_errmsg()).toStdString() << std::endl;
        exit(0);
    }
    if(argc == 2)                                  // No parameters except for "-no-gui"
    {
        m_usage = true;
        return;
    }
    for(int i = 1; i < argc; i++)                  // Go through all command-line parameters.
    {
        // Check for help parameters
        if(strcmp("-h", argv[i]) == 0 || strcmp("--help", argv[i]) == 0)
        {
            m_usage = true;
            return;
        }
        else if(strcmp("--raw", argv[i]) == 0)     // Check for raw format.
            m_format |= OutputFormat::Raw;
        else if(strcmp("--ini", argv[i]) == 0)     // Check for INI format.
            m_format |= OutputFormat::Ini;
        else if(strcmp("-o", argv[i]) == 0 && i + 1 < argc)  // Check if output file is specified.
        {
            m_filename = argv[i + 1];
            i++;
        }
        else if(strcmp("--no-gui", argv[i]) == 0) {}  // Skip "--no-gui" option.
        else                                       // Parameter must be filename.
        {
            QFileInfo info(argv[i]);
            if(info.exists())                      // Check if file exists.
            {
                if(info.isFile())                  // Is file and not a directory.
                    m_media.push_back(new Media(argv[i]));
                else                               // Is directory, add all files.
                {
                    QStringList paths = QDir(argv[i]).entryList(QDir::Files);
                    foreach(QString path, paths)
                        m_media.push_back(new Media(QString(argv[i]) + "/" + path));
                }
            }
            else                                   // File doesn't exist, print error message.
                std::cout << QObject::tr("%1 - no such file or directory.").arg(argv[i]).toStdString() << std::endl;
        }
    }
}


int Cli::exec()                                    // Execute application.
{
    if(m_usage)                                    // Print usage and quit.
    {
        printUsage();
        return 0;
    }
    else if(m_media.size() > 0)                    // Write information about media files.
    {
        if(m_format & OutputFormat::Raw ||         // Use raw format or no format specified.
           m_format == OutputFormat::Default)
        {
            if(writeRaw() != 0)
                return 1;
        }
        if(m_format & OutputFormat::Ini)           // Use INI format.
        {
            if(writeIni() != 0)
                return 1;
        }
    }
    return 0;
}


void Cli::printUsage() const
{
    std::cout << QObject::tr("vlcinfo [OPTIONS] [FILES]").toStdString() << std::endl << std::endl
            << QObject::tr("Options:").toStdString() << std::endl
            << QObject::tr("\t-h, --help = display this information").toStdString() << std::endl
            << QObject::tr("\t--no-gui = use program in CLI-mode").toStdString() << std::endl
            << QObject::tr("\t-o filename = specify output filename").toStdString() << std::endl
            << QObject::tr("\t--raw = print information in plain text format").toStdString() << std::endl
            << QObject::tr("\t--ini = print information in INI-format (suffix: .ini)").toStdString() << std::endl << std::endl;
}


int Cli::writeRaw()                                 // Write raw information of all media files to output file or stdout.
{
    QFile file;
    if(!m_filename.isEmpty())                       // Filename specified.
    {
        file.setFileName(m_filename);
        if(!file.open(QIODevice::WriteOnly | QIODevice::Text))  // Create text file with that name.
        {
            std::cout << "Error creating file: " << file.errorString().toStdString() << std::endl;
            return 1;
        }
    }
    else                                            // No filename specified, write to stdout.
    {
        if(!file.open(stdout, QIODevice::WriteOnly | QIODevice::Text))
        {
            std::cout << "Error writing to stdout: " << file.errorString().toStdString() << std::endl;
            return 1;
        }
    }
    foreach(Media *m, m_media)                      // Write information about each media.
        writeRaw(*m, file);
    return 0;
}


void Cli::writeRaw(const Media& media, QFile& file)   // Write raw information of a single media file to output file or stdout.
{
    QTextStream stream(&file);
    media.saveToFile(file);
}


int Cli::writeIni()                                   // Write information in INI format of all media files to output file or stdout.
{
    bool tmp = false;
    if(m_filename.isEmpty())                          // No filename specified, use a temporary file.
    {
        m_filename = QTemporaryFile().fileName();
        tmp = true;
    }
    m_filename += ".ini";                             // Use ".ini" suffix for file.
    QSettings *settings = new QSettings(m_filename, QSettings::IniFormat);
    foreach(Media *m, m_media)                        // Create data for each media file.
        writeIni(*m, *settings);
    delete settings;
    if(tmp)                                           // Temporary file was used, read data from it and delete it.
    {
        QFile file(m_filename);
        if(!file.open(QIODevice::ReadOnly | QIODevice::Text))  // Open text file for reading.
        {
            std::cout << "Error opening temporary file: " << file.errorString().toStdString() << std::endl;
            return 1;
        }
        QTextStream t(&file);
        QString line;
        do                                            // Read each line and print it.
        {
            line = t.readLine();
            std::cout << line.toStdString() << std::endl;
        }while(!line.isNull());
        file.remove();                                // Delete file.
    }
    return 0;
}


void Cli::writeIni(const Media& media, QSettings& settings)  // Write information in INI format of a single media file to output file or stdout.
{
    QPair<unsigned int, unsigned int> typedep;
    const QList<TrackInfo *> tracks = media.mediaInfo().trackInfo();
    QFileInfo info(media.filename());

    settings.beginGroup(info.fileName());

    settings.beginGroup(QObject::tr("General Information"));
    settings.setValue(QObject::tr("File Name"), info.fileName());
    settings.setValue(QObject::tr("Path"), info.path());
    settings.setValue(QObject::tr("Container Type"), media.containerType());
    settings.setValue(QObject::tr("Size"), QString::number((qreal)info.size() / 1000000, 'g', 3) + " MB");
    settings.setValue(QObject::tr("Created"), info.created().toString("dd/MM/yyyy hh:mm"));
    settings.endGroup();

    settings.beginGroup(QObject::tr("Tags"));
    if(!media.metaInfo().title().isEmpty())
        settings.setValue(QObject::tr("Title"), media.metaInfo().title());
    if(!media.metaInfo().album().isEmpty())
        settings.setValue(QObject::tr("Album"), media.metaInfo().album());
    if(!media.metaInfo().artist().isEmpty())
        settings.setValue(QObject::tr("Artist"), media.metaInfo().artist());
    if(!media.metaInfo().trackNumber().isEmpty())
        settings.setValue(QObject::tr("Tracknumber"), media.metaInfo().trackNumber());
    if(!media.metaInfo().date().isEmpty())
        settings.setValue(QObject::tr("Date"), media.metaInfo().date());
    if(!media.metaInfo().genre().isEmpty())
        settings.setValue(QObject::tr("Genre"), media.metaInfo().genre());
    if(!media.metaInfo().copyright().isEmpty())
        settings.setValue(QObject::tr("Copyright"), media.metaInfo().copyright());
    if(!media.metaInfo().rating().isEmpty())
        settings.setValue(QObject::tr("Rating"), media.metaInfo().rating());
    if(!media.metaInfo().description().isEmpty())
        settings.setValue(QObject::tr("Description"), media.metaInfo().description());
    if(!media.metaInfo().language().isEmpty())
        settings.setValue(QObject::tr("Language"), media.metaInfo().language());
    if(!media.metaInfo().setting().isEmpty())
        settings.setValue(QObject::tr("Setting"), media.metaInfo().setting());
    if(!media.metaInfo().url().isEmpty())
        settings.setValue(QObject::tr("URL"), media.metaInfo().url());
    if(!media.metaInfo().nowPlaying().isEmpty())
        settings.setValue(QObject::tr("Now Playing"), media.metaInfo().nowPlaying());
    if(!media.metaInfo().publisher().isEmpty())
        settings.setValue(QObject::tr("Publisher"), media.metaInfo().publisher());
    if(!media.metaInfo().encodedBy().isEmpty())
        settings.setValue(QObject::tr("Encoded By"), media.metaInfo().encodedBy());
    if(!media.metaInfo().artworkUrl().isEmpty())
        settings.setValue(QObject::tr("Artwork-URL"), media.metaInfo().artworkUrl());
    if(!media.metaInfo().trackId().isEmpty())
        settings.setValue(QObject::tr("Track-ID"), media.metaInfo().trackId());
    settings.endGroup();

    settings.beginGroup(QObject::tr("Media Information"));
    settings.setValue(QObject::tr("Duration"), media.mediaInfo().durationString());
    for(int i = 0; i < media.mediaInfo().numTracks(); i++)
    {
        settings.beginGroup(QString::number(i));
        settings.setValue(QObject::tr("Type"), tracks[i]->type().toString());
        settings.setValue(QObject::tr("Codec"), tracks[i]->codecString());
        settings.setValue(QObject::tr("ID"), tracks[i]->id());
        settings.setValue(QObject::tr("Profile"), tracks[i]->profile());
        settings.setValue(QObject::tr("Level"), tracks[i]->level());
        typedep = tracks[i]->typeDependentInfo();
        if(tracks[i]->vlcType() == libvlc_track_audio)
        {
            settings.setValue(QObject::tr("Channels"), typedep.first);
            settings.setValue(QObject::tr("Sample Rate"),typedep.second);
        }
        else if(tracks[i]->vlcType() == libvlc_track_video)
        {
            settings.setValue(QObject::tr("Width"), typedep.first);
            settings.setValue(QObject::tr("Height"),typedep.second);
        }
        settings.endGroup();
    }
    settings.endGroup();

    settings.endGroup();
}
