#ifndef MEDIA_H
#define MEDIA_H

#include <QMap>
#include <QString>
#include <QObject>
#include <QTextStream>
#include <QFile>
#include <QFileInfo>
#include <QDateTime>
#include "VLC.h"
#include "MetaInfo.h"
#include "TrackInfo.h"

// Class to get the container type form the file suffix.
class ContainerType
{
public:
    static void init();                               // Initialize map with according values.
    static bool isInit() { return !sm_typemap.isEmpty(); }  // Return true if map is initialized.
    static QString typeOf(const QString& suffix);     // Return the type of the container as a string.
private:
    static QMap<QString, QString> sm_typemap;         // Map that contains the types and suffixes
};


// Media-information of a media-file.
class MediaInfo
{
public:
    MediaInfo(libvlc_media_t *const m);           // Media-file is passed as a parameter.
    ~MediaInfo();
    const qint64& duration() const { return m_duration; }               // Return duration of the media-file in milliseconds.
    const QString& durationString() const { return m_durationstring; }  // Return duration as a string in this format: (h)h:mm:ss
    int numTracks() const { return m_trackinfo.size(); }                // Number of tracks in the media.
    const QList<TrackInfo *>& trackInfo() const { return m_trackinfo; } // Return list of track-information of the media.
    bool isEmpty() const { return m_duration == 0 && m_trackinfo.size() == 0; }  // Return if there actually is any media info.
private:
    QString m_durationstring;                     // Duration as a string.
    qint64 m_duration;                            // Duration in milliseconds.
    QList<TrackInfo *> m_trackinfo;               // List of track-information of the media.
};


// Represents a single media-file.
class Media
{
public:
    Media(const QString& filename);          // Filename is passed as parameter.
    ~Media();
    const QString& filename() const { return m_filename; }       // Return the filename.
    const QString& containerType() const { return m_containertype; } // Return container type.
    const MetaInfo& metaInfo() const { return *m_metainfo; }     // Return meta-information (tags) of the file.
    const MediaInfo& mediaInfo() const { return *m_mediainfo; }  // Return media-information of the file.
    bool isValid() const;                                        // Return true if media is valid.
    void saveToFile(QFile& file) const;                          // Write information to passed file.
private:
    libvlc_media_t *m_media;                 // libVLC representation of the media-file.
    QString m_filename,                      // Name of the media-file.
            m_containertype;                 // Container type according to file suffix.
    MetaInfo *m_metainfo;                    // Meta-information of the file.
    MediaInfo *m_mediainfo;                  // Media-information of the file.
};

#endif // MEDIA_H
