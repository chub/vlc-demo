#ifndef VLC_H
#define VLC_H

#include <cstdio>
#include <vlc/vlc.h>

// Class to initialize and shutdown libVLC.
class VLC
{
public:
    static int init();                      // Initialize libVLC, return value != 0 is error.
    static void quit();                     // Shutdown libVLC.
    static libvlc_instance_t *instance() { return sm_instance; }   // Return instance of libVLC.
private:
    static libvlc_instance_t *sm_instance;  // Instance of libVLC.
};

#endif // VLC_H
