#include "MediaWidget.h"

MediaWidget::MediaWidget(Media *const m, QWidget *parent) : QTreeWidget(parent), m_media(m) // Media and parent are passed as parameter.
{
    createTopNodes();                                     // Create top-level nodes of the information tree.
    createGeneralTree(m);                                 // Add information to the information-tree.
    createTagTree(m);
    if(!m_media->mediaInfo().isEmpty())
        createMediaInfoTree(m);
    expandAll();                                          // Expand all items.
    setHeaderHidden(true);                                // Hide header.
    setHorizontalScrollBarPolicy(Qt::ScrollBarAsNeeded);  // Ensure horizontal scrollbar when content is too large.
    header()->setStretchLastSection(false);
    header()->setSectionResizeMode(QHeaderView::ResizeToContents);
}


void MediaWidget::createTopNodes()                      // Create top-level nodes of the information tree.
{
    m_generalnode = new QTreeWidgetItem(this);
    QFont boldfont = m_generalnode->font(0);
    boldfont.setBold(true);
    boldfont.setPointSize(boldfont.pointSize() + 2);
    m_generalnode->setText(0, tr("General Information"));
    m_generalnode->setFont(0, boldfont);
    addTopLevelItem(m_generalnode);
    m_tagnode = new QTreeWidgetItem(this);
    m_tagnode->setText(0, tr("Meta-Tags"));
    m_tagnode->setFont(0, boldfont);
    addTopLevelItem(m_tagnode);
    if(!m_media->mediaInfo().isEmpty())
    {
        m_mediainfonode = new QTreeWidgetItem(this);
        m_mediainfonode->setText(0, tr("Media Information"));
        m_mediainfonode->setFont(0, boldfont);
        addTopLevelItem(m_mediainfonode);
    }
}


void MediaWidget::createGeneralTree(Media *const m)              // Add general information to the informatio tree.
{
    QFileInfo info(m->filename());
    QTreeWidgetItem *item;
    item = new QTreeWidgetItem(m_generalnode);
    item->setText(0, tr("File Name: ") + info.fileName());;
    item = new QTreeWidgetItem(m_generalnode);
    item->setText(0, tr("Path: ") + info.path());
    item = new QTreeWidgetItem(m_generalnode);
    item->setText(0, tr("Container Type: ") + m->containerType());
    item = new QTreeWidgetItem(m_generalnode);
    item->setText(0, tr("Size: ") + QString::number((qreal)info.size() / 1000000, 'g', 3) + " MB");
    item = new QTreeWidgetItem(m_generalnode);
    item->setText(0, tr("Created: ") + info.created().toString("dd/MM/yyyy hh:mm"));
}


void MediaWidget::createTagTree(Media *const m)                  // Add tags to information tree.
{
    const MetaInfo& meta = m->metaInfo();
    QTreeWidgetItem *item;
    if(!meta.title().isEmpty())
    {
        item = new QTreeWidgetItem(m_tagnode);
        item->setText(0, tr("Title: ") + meta.title());
    }
    if(!meta.album().isEmpty())
    {
        item = new QTreeWidgetItem(m_tagnode);
        item->setText(0, tr("Album: ") + meta.album());
    }
    if(!meta.artist().isEmpty())
    {
        item = new QTreeWidgetItem(m_tagnode);
        item->setText(0, tr("Artist: ") + meta.artist());
    }
    if(!meta.trackNumber().isEmpty())
    {
        item = new QTreeWidgetItem(m_tagnode);
        item->setText(0, tr("Tracknumber: ") + meta.trackNumber());
    }
    if(!meta.date().isEmpty())
    {
        item = new QTreeWidgetItem(m_tagnode);
        item->setText(0, tr("Date: ") + meta.date());
    }
    if(!meta.genre().isEmpty())
    {
        item = new QTreeWidgetItem(m_tagnode);
        item->setText(0, tr("Genre: ") + meta.genre());
    }
    if(!meta.copyright().isEmpty())
    {
        item = new QTreeWidgetItem(m_tagnode);
        item->setText(0, tr("Copyright: ") + meta.copyright());
    }
    if(!meta.rating().isEmpty())
    {
        item = new QTreeWidgetItem(m_tagnode);
        item->setText(0, tr("Rating: ") + meta.rating());
    }
    if(!meta.description().isEmpty())
    {
        item = new QTreeWidgetItem(m_tagnode);
        item->setText(0, tr("Description: ") + meta.description());
    }
    if(!meta.language().isEmpty())
    {
        item = new QTreeWidgetItem(m_tagnode);
        item->setText(0, tr("Language: ") + meta.language());
    }
    if(!meta.setting().isEmpty())
    {
        item = new QTreeWidgetItem(m_tagnode);
        item->setText(0, tr("Setting: ") + meta.setting());
    }
    if(!meta.url().isEmpty())
    {
        item = new QTreeWidgetItem(m_tagnode);
        item->setText(0, tr("URL: ") + meta.url());
    }
    if(!meta.nowPlaying().isEmpty())
    {
        item = new QTreeWidgetItem(m_tagnode);
        item->setText(0, tr("Now Playing: ") + meta.nowPlaying());
    }
    if(!meta.publisher().isEmpty())
    {
        item = new QTreeWidgetItem(m_tagnode);
        item->setText(0, tr("Publisher: ") + meta.publisher());
    }
    if(!meta.encodedBy().isEmpty())
    {
        item = new QTreeWidgetItem(m_tagnode);
        item->setText(0, tr("Encoded By: ") + meta.encodedBy());
    }
    if(!meta.artworkUrl().isEmpty())
    {
        item = new QTreeWidgetItem(m_tagnode);
        item->setText(0, tr("Artwork-URL: ") + meta.artworkUrl());
    }
    if(!meta.trackId().isEmpty())
    {
        item = new QTreeWidgetItem(m_tagnode);
        item->setText(0, tr("Track-ID: ") + meta.trackId());
    }
}


void MediaWidget::createMediaInfoTree(Media *const m)            // Add media-information to the information tree.
{
    QTreeWidgetItem *tracknode, *item;
    QPair<unsigned int, unsigned int> typedep;
    const MediaInfo& media = m->mediaInfo();
    const QList<TrackInfo *> tracks = media.trackInfo();
    int numtracks = tracks.size();
    if(media.duration() > 0)
    {
        item = new QTreeWidgetItem(m_mediainfonode);
        item->setText(0, tr("Duration: ") + media.durationString());
    }

    for(int i = 0; i < numtracks; i++)
    {
        tracknode = new QTreeWidgetItem(m_mediainfonode);
        tracknode-> setText(0, tr("Track #") + QString::number(i));
        item = new QTreeWidgetItem(tracknode);
        item->setText(0, tr("Type: ") + tracks[i]->type().toString());
        item = new QTreeWidgetItem(tracknode);
        item->setText(0, tr("Codec: ") + tracks[i]->codecString());
        item = new QTreeWidgetItem(tracknode);
        item->setText(0, tr("ID: ") + QString::number(tracks[i]->id()));
        item = new QTreeWidgetItem(tracknode);
        item->setText(0, tr("Profile: ") + QString::number(tracks[i]->profile()));
        item = new QTreeWidgetItem(tracknode);
        item->setText(0, tr("Level: ") + QString::number(tracks[i]->level()));
        typedep = tracks[i]->typeDependentInfo();
        if(tracks[i]->vlcType() == libvlc_track_audio)
        {
            item = new QTreeWidgetItem(tracknode);
            item->setText(0, tr("Channels: ") + QString::number(typedep.first));
            item = new QTreeWidgetItem(tracknode);
            item->setText(0, tr("Sample Rate: ") + QString::number(typedep.second) + tr(" Hz"));
        }
        else if(tracks[i]->vlcType() == libvlc_track_video)
        {
            item = new QTreeWidgetItem(tracknode);
            item->setText(0, tr("Width: ") + QString::number(typedep.first));
            item = new QTreeWidgetItem(tracknode);
            item->setText(0, tr("Height: ") + QString::number(typedep.second));
        }
    }
}
