#include "VLC.h"

libvlc_instance_t *VLC::sm_instance = NULL;   // Instance of libVLC.

int VLC::init()                               // Initialize libVLC
{
    static const char * const opt [] = {
        //"--verbose=2",
        "--vout=dummy",
        "--aout=dummy",
    };
    if(sm_instance)                           // Check if already initialized.
        return 0;
    sm_instance = libvlc_new(sizeof opt / sizeof *opt, opt);  // Initialize libVLC.
    return sm_instance == NULL ? 1 : 0;       // Return 0 if initialization was successful.
}


void VLC::quit()                              // Shutdown libVLC.
{
    libvlc_release(sm_instance);
}
