#ifndef APPLICATION_H
#define APPLICATION_H

#include "Cli.h"
#include "MainWindow.h"
#include <QApplication>
#include <cstring>

// Class checking if CLI or GUI mode is requested.
class Application
{
public:
    Application(int argc, char *argv[]);  // Constructor that evaluates the command line parameters.
    ~Application();                       // Clean up
    int exec();                           // Execute either GUI or CLI application.
private:
    Cli *m_cli;                           // CLI application
    QApplication *m_app;                  // GUI application
    MainWindow *m_mainwindow;             // GUI mainwindow
};

#endif // APPLICATION_H
