#ifndef MAINWIDGETS_H
#define MAINWIDGETS_H

#include <QApplication>
#include <QTabWidget>
#include <QFileInfo>
#include <QFile>
#include <QMessageBox>
#include <QPushButton>
#include <QShortcut>
#include <QVBoxLayout>
#include <QLabel>
#include "MediaWidget.h"

static const QString iconpath = ":/icon.png";         // Path to icon.

// Widget that is shown when no tab is opened.
class StartWidget : public QWidget
{
    Q_OBJECT
public:
    StartWidget(QWidget *parent);
private:
    QPushButton *m_openbutton, *m_opendirbutton;      // Buttons to open files and directories.
    QHBoxLayout *m_buttonlayout;
    QVBoxLayout *m_mainlayout;
    QLabel *m_iconlabel;                              // Label containing the icon.
};


// Mainwidget of the application that shows all information in tabs.
class TabWidget : public QTabWidget
{
    Q_OBJECT
public:
    TabWidget(QWidget *parent);
    ~TabWidget();
    void addMedia(const QString& filename);    // Add media with the passed name to the mainwindow.
private slots:
    void removeMedia();                        // Remove current media from the mainwindow.
    void removeMedia(int index);               // Remove media with the passed tab-index from the mainwindow.
    void saveToFile();
private:
    QList<Media *> m_mediafiles;               // List with information about the loaded files.
    QList<MediaWidget *> m_mediawidgets;       // Widgets showing the information about the loaded files.
    int indexOf(const QString& filename);      // Return the tab-index of the file with the passed name.
    QShortcut *m_shortcut;
signals:
    void lastTabClosed();                      // Emitted when last tab is closed.
    void firstTabOpened();                     // Emitted when first tab is opened.
};

#endif // MAINWIDGETS_H
